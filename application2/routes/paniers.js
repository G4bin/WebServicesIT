var express = require('express');
var router = express.Router();
const axios = require('axios');
var app = express.Application = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var io_client = require('socket.io-client');
var socket = io_client("ws://localhost:3000");

let paniers = [];

socket.on("validateBuy", function(msg) {
    console.log("panier validated.", msg)
});

router.get('/', function (req, res, next) {
    res.send(paniers);
});

router.post('/', function (req, res, next) {
    let panier = {};
    panier.idBook = req.body.idBook;

    axios.get(`http://127.0.0.1:3000/books/${panier.idBook}`)
        .then(function (response) {
            panier.id = paniers.length;
            panier.date = new Date();
            paniers.push(panier);
            res.send({ message: 'panier créé' });
        })
        .catch(function (error) {
            res.status(400).send({ message: 'panier non créé', error });
        })
});

router.put('/:id/valider', function (req, res, next) {
    let panier = null;
    paniers.forEach(p => {
        if(p.id === parseInt(req.params.id)) {
            panier = p;
        }
    })
    if(panier !== null) {
        io.send(JSON.stringify({ action: "acheter", idBook: panier.idBook }));

        res.send({ message: 'panier validé' });
    } else {
        res.status(404).send({error: 'panier introuvable'});
    }
});

module.exports = router;
