var express = require('express');
var router = express.Router();
var app = express.Application = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var io_client = require('socket.io-client');
var socket = io_client("ws://localhost:3001");

let books = [];

socket.on("buy", function (msg) {
    let body = JSON.parse(msg.toString());
    books.forEach(book => {
        if (book.id === parseInt(body.idBook)) {
            if (book.stock > 1) {
                book.stock--;
                io.send(JSON.stringify({ action: "validateBuy", idBook: body.idBook }))
            } else {
                return;
            }
        }
    })
});

router.get('/', function (req, res, next) {
    res.send(books);
});

router.post('/', function (req, res, next) {
    let book = {
        id: books.length,
        titre: req.body.titre,
        auteur: req.body.auteur,
        prix : 5,
        stock: 10,
    };

    books.push(book);
    res.send({ message: 'Le livre a été créé' });
});

router.get('/:id', function (req, res, next) {
    let book = null;
    books.forEach(b => {
        if (b.id === parseInt(req.params.id)) {
            book = b;
        }
    })
    if (book !== null) {
        res.send(book)
    } else {
        res.status(404).send({ error: 'Ce livre est introuvable.' });
    }
});

router.post('/:id/acheter', function (req, res, next) {
    books.forEach(book => {
        if (book.id === parseInt(req.params.id)) {
            if (book.stock > 1) {
                book.stock--;
            } else {
                res.status(400).send({ message: 'Pas assez de stock' });
                return;
            }
        }
    })

    
    res.send({ message: 'Livre acheté' });
});

module.exports = router;
